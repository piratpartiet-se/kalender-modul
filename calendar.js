$(document).ready(function () {
	$("#calendar").fullCalendar({
		themeSystem: 'bootstrap4',
		editable: false,
		height: "parent",
		weekNumbers: true,
		firstDay: 1,
		locale: "sv",
		buttonText: {
			list: "Agenda"
		},
		timezone: "Europe/Stockholm",
		eventRender: function(eventObj, $el) {
			var content = '<b>Tid: </b>'
						+ eventObj.start.format("HH:mm")
						+ " - "
						+ eventObj.end.format("HH:mm");

			if (eventObj.description !== "" && eventObj.location !== "") {
				content += "\n<b>Plats:</b> " + eventObj.location;
				content += "\n\n" + eventObj.description;
			} else if (eventObj.description !== "") {
				content += "\n<b>Plats:</b> <i>Okänd</i>";
				content += "\n\n" + eventObj.description;
			} else if (eventObj.location !== "") {
				content += "\n<b>Plats:</b> " + eventObj.location;
				content += "\n\n<i>Ingen beskrivning</i>";
			} else {
				content += "\n<b>Plats:</b> <i>Okänd</i>";
				content += "\n\n<i>Ingen beskrivning</i>";
			}

			$el.popover({
				html: true,
				title: eventObj.title,
				content: content,
				trigger: 'hover',
				placement: 'top',
				container: 'body'
			});
		},
	});
	$.get($("#calendar").data("src"), function (res) {
		var events = [];
		var parsed = ICAL.parse(res);
		var baseTimezone = "Z";
		var daylightTimezone = false;

		parsed[2].forEach(function(event) {
			switch (event[0]) {
				case "vtimezone":
					baseTimezone = event[2][0][1][4][3];
					if (typeof event[2][1] !== "undefined") {
						daylight = event[2][1][1][4][3];
					}
					break;
				case "vevent":
					var summary, location, start, end, description;
					event[1].forEach(function(event_item) {
						switch(event_item[0]) {
							case "location":
								location = event_item[3];
								break;
							case "summary":
								summary = event_item[3];
								break;
							case "dtstart":
								if (event_item[2] === "date-time") {
									start = event_item[3].replace("Z", baseTimezone);
									start = moment(start);
									if (daylightTimezone !== false) {
										start = moment(start).zone(daylightTimezone);
									}
								} else {
									start = event_item[3];
								}
								break;
							case "dtend":
								if (event_item[2] === "date-time") {
									end = event_item[3].replace("Z", baseTimezone);
									end = moment(end);
									if (daylightTimezone !== false) {
										end = moment(end).zone(daylightTimezone);
									}
								} else {
									end = event_item[3];
								}
								break;
							case "description":
								description = event_item[3];
								break;
						}
					});
					if (summary && start && end) {
						events.push({
							title: summary,
							start: start,
							end: end,
							location: location,
							description: description
						});
					}
					break;

				default:
					console.log(event[0] + " eventtype is not yet implemented.", event);
					break;
			}
		});
		$("#calendar").fullCalendar("removeEventSources");
		$("#calendar").fullCalendar("addEventSource", events);
	});
});